import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
    providedIn: 'root'
})
export class BookingApi {
    private readonly apiController: string = 'apis';

    constructor(private api: HttpService) { }


    demoTesting() {
        return this.api.get(`${this.apiController}/demoTesting`);
    }

    login(email,password) {
        return this.api.post(`${this.apiController}/login`,{email,password});
    }

    getCustomerTypes(){
        return this.api.get(`${this.apiController}/getCustomerTypes`);
    }

    getCustomerHistory(type){
        return this.api.get(`${this.apiController}/getCustomerHistory?type=`+type);
    }

    getZones(){
        return this.api.get(`${this.apiController}/getZones`);
    }

    getAddressMasters(){
        return this.api.get(`${this.apiController}/getAddressMasters`);
    }

    getPincodeMasters(){
        return this.api.get(`${this.apiController}/getPincodeMasters`);
    }

    getOrdersHistoryMasters(type){
        return this.api.get(`${this.apiController}/getOrdersHistoryMasters?type=`+type);
    }

    getOrdersStatusMasters(type){
        return this.api.get(`${this.apiController}/getOrdersStatusMasters?type=`+type);
    }

    getPaytypeMasters(type){
        return this.api.get(`${this.apiController}/getPaytypeMasters?type=`+type);
    }

    getDeliveryMasters(){
        return this.api.get(`${this.apiController}/getDeliveryMasters`);
    }

    getFlagMasters(){
        return this.api.get(`${this.apiController}/getFlagMasters`);
    }

    getRecommendationConfig(){
        return this.api.get(`${this.apiController}/getRecommendationConfig`);
    }

    updateRecommendation(item){
        return this.api.put(`${this.apiController}/updateRecommendation`,item);
    }

    deleteRecommendation(_id){
        return this.api.delete(`${this.apiController}/deleteRecommendation?_id=`+_id);
    }

    saveRecommendation(item){
        return this.api.post(`${this.apiController}/saveRecommendation`,item)
    }

    getRecommendation(_id){
        return this.api.get(`${this.apiController}/getRecommendation?_id=`+_id);
    }

    getCustomerProfile(item){
        return this.api.post(`${this.apiController}/getCustomerProfile`,item)
    }
}