import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BookingApi } from '@core/api/booking';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  customerTypes: any;
  customerHistory: any;
  zones: any;
  zone: any;
  address: any;
  pincodes: any;
  ordersHistory: any;
  ordersStatus: any;
  deliverys: any;
  flags: any;
  payTypes: any;
  bookingForm: FormGroup
  pay_type: any;
  flag: any;
  recommendationData: any;
  addNewFlag: boolean = false;
  updateRecommendation: any;
  removeRecommendation: any;
  bookingData = {}
  saveRecommendation: any;
  customer_type: any;
  getRecommendation: any;
  customer_history: any;
  addressMasters: any;
  pincode_performance: any;
  order_history: any;
  order_status: any;
  delivery_possibility: any;
  editFlag: boolean = false;
  recomm_id: any;
  updateData = {};
  deleteFlag: boolean = false;
  pincodePerformanceData: any;
  customerPerformanceData: any;
  showInputLessAddress: boolean = false;
  addressLengthCtrl = new FormControl(0);
  showInputPerformance: boolean = false;
  pincodeGreaterCtrl = new FormControl(0);
  pincodeLessCtrl = new FormControl(0);
  pincodeGreater: number;
  pincodeLess: number;
  pinPerformance: any;
  addressLength: number;
  addressValue: any;
  showInputStatus: boolean = false;
  statusGreaterCtrl = new FormControl(0);
  statusLessCtrl = new FormControl(0)
  orderHistoryGreaterCtrl = new FormControl(0)
  statusGreater: number;
  statusLess: number;
  orderStatus: any;
  address_length: any;
  pincode_greater: any;
  pincode_less: any;
  status_greater: any;
  status_less: any;
  type: any;
  addressEvent: any;
  showInputGreaterAddress: boolean = false;
  showAddressDropDown: boolean = true; 
  orderHistoryEvent: any;
  orderHistoryGreater: any;
  orderHistoryValue: string;
  showInputGreaterOrderHistory: boolean = false;
  showOrderHistoryDropDown: boolean = true;
  order_history_greater: any;
  pincodeEvent: any;
  orderStatusEvent: any;
  recomLength: any;
  tempData = [];
  temp = [];
  @ViewChild(DatatableComponent) table: DatatableComponent;
  searchValue = '';
  

  constructor(private bookingApi: BookingApi,private formBuilder:FormBuilder,
    private _toastrService: ToastrService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.bookingForm = this.formBuilder.group({
      typeCtrl:['',Validators.required],
      historyCtrl:['',Validators.required],
      zoneCtrl:['',Validators.required],
      addressCtrl:[''],
      addressLengthCtrl: [0],
      pincodeCtrl:[''],
      pincodeGreaterCtrl: [0],
      pincodeLessCtrl: [0],
      orderHistoryCtrl:[''],
      orderHistoryGreaterCtrl: [0],
      orderStatusCtrl:[''],
      statusGreaterCtrl: [0],
      statusLessCtrl: [0],
      paytypeCtrl:['',Validators.required],
      deliveryCtrl:['',Validators.required],
      flagCtrl:['',Validators.required],
   })

    this.bookingApi.getCustomerTypes().subscribe(data => {
      this.customerTypes = data['data'];
      // console.log("customerTypes---",this.customerTypes)
    });

    

    this.bookingApi.getZones().subscribe(data => {
      this.zones = data['data'];
    });

    this.bookingApi.getPincodeMasters().subscribe(data => {
      this.pincodes = data['data'];
    });

    this.bookingApi.getDeliveryMasters().subscribe(data => {
      this.deliverys = data['data'];
    });

    this.bookingApi.getFlagMasters().subscribe(data => {
      this.flags = data['data'];
    });

    this.bookingApi.getAddressMasters().subscribe(data => {
      this.addressMasters = data['data'];
    });

    this.bookingApi.getRecommendationConfig().subscribe(data => {
      this.recommendationData = data['data'];
      this.tempData = this.recommendationData
      this.recomLength = this.recommendationData.length
      // console.log("recommendationData--",this.recommendationData)
    });

  }

  selectType(event){
    console.log("type--",event)
    this.type = event;
    this.bookingApi.getCustomerHistory(this.type).subscribe(data => {
      this.customerHistory = data['data'];
      console.log("customerHistory---",this.customerHistory)
    });

    this.bookingApi.getOrdersHistoryMasters(this.type).subscribe(data => {
      this.ordersHistory = data['data'];
    });

    this.bookingApi.getPaytypeMasters(this.type).subscribe(data => {
      this.payTypes = data['data'];
    });

    this.bookingApi.getOrdersStatusMasters(this.type).subscribe(data => {
      this.ordersStatus = data['data'];
      // console.log("this.ordersStatus---",this.ordersStatus)
    });
    if(this.type == 'New'){
      this.addressEvent = 'length';
      this.showInputGreaterAddress = true;
      this.showAddressDropDown = false;
      // this.orderHistoryEvent = 'orderHistoryGreater';
      // this.showInputGreaterOrderHistory = true;
      // this.showOrderHistoryDropDown = false

    }else{
      this.showInputGreaterAddress = false;
      this.showAddressDropDown = true;
      // this.showInputGreaterOrderHistory = false;
      // this.showOrderHistoryDropDown = true;
    }
  }

  selectHistory(event){
    console.log("history--",event)

  }

  selectZone(event){
    console.log("zone--",event)
    // this.zone = event

  }

  selectAddress(event) {
    this.addressEvent = event
    console.log("address--", event)
  }

  selectPincode(event){
    console.log("pincode--",event)
    this.pincodeEvent = event;
    if (event == 'pincodePerformance') {
      this.showInputPerformance = true;
    }else if (event != 'pincodePerformance' && event != '') {
      this.showInputPerformance = false;
    }
   
  }

  selectOrderHistory(event){
    console.log("orderhistory--",event)
    this.orderHistoryEvent = event;
    if (this.orderHistoryEvent == 'orderHistoryGreater') {
      this.showInputGreaterOrderHistory = true;
    }else if (this.orderHistoryEvent != 'orderHistoryGreater' && event != '') {
      this.showInputGreaterOrderHistory = false;
    }

  }

  selectOrderStatus(event){
    console.log("orderstatus--",event)
    this.orderStatusEvent = event;
    if (event == 'orderPercentage') {
      this.showInputStatus = true;
    }else if (event != 'orderPercentage' && event != '') {
      this.showInputStatus = false;
    }
  }

  selectPaytype(event){
    console.log("paytype--",event)
    // this.pay_type = event
  }

  selectDelivery(event){
    console.log("delivery--",event)
  }

  selectFlag(event){
    console.log("flag--",event)
  }

  addNew(){
    this.addNewFlag = true;
    this.editFlag = false;
    this.showInputStatus = false;
    this.showInputPerformance = false;
    this.showInputGreaterAddress = false;
    this.showInputGreaterOrderHistory = false;
    this.showAddressDropDown = true;
    this.bookingForm.get('typeCtrl').reset();
    this.bookingForm.get('historyCtrl').reset();
    this.bookingForm.get('zoneCtrl').reset();
    this.bookingForm.get('addressCtrl').reset();
    this.bookingForm.get('pincodeCtrl').reset();
    this.bookingForm.get('orderHistoryCtrl').reset();
    this.bookingForm.get('orderStatusCtrl').reset();
    this.bookingForm.get('paytypeCtrl').reset();
    this.bookingForm.get('deliveryCtrl').reset();
    this.bookingForm.get('flagCtrl').reset();
    this.bookingForm.get('orderHistoryGreaterCtrl').reset();
    this.bookingForm.get('addressLengthCtrl').reset();
    this.bookingForm.get('pincodeGreaterCtrl').reset();
    this.bookingForm.get('pincodeLessCtrl').reset();
    this.bookingForm.get('statusGreaterCtrl').reset();
    this.bookingForm.get('statusLessCtrl').reset();
  }

  Save(){
    this.bookingForm.controls.typeCtrl.markAsDirty();
    this.bookingForm.controls.historyCtrl.markAsDirty();
    this.bookingForm.controls.zoneCtrl.markAsDirty();
    this.bookingForm.controls.addressCtrl.markAsDirty();
    this.bookingForm.controls.pincodeCtrl.markAsDirty();
    this.bookingForm.controls.orderHistoryCtrl.markAsDirty();
    this.bookingForm.controls.orderStatusCtrl.markAsDirty();
    this.bookingForm.controls.paytypeCtrl.markAsDirty();
    this.bookingForm.controls.deliveryCtrl.markAsDirty();
    this.bookingForm.controls.flagCtrl.markAsDirty();
    console.log("this.bookingForm.get('pincodeCtrl').value---",this.bookingForm.get('pincodeCtrl').value)

    if(this.bookingForm.get('pincodeCtrl').value == 'pincodePerformance' || this.bookingForm.get('pincodeCtrl').value == ''){
      this.bookingForm.get('pincodeGreaterCtrl').addValidators(Validators.required); 
      this.bookingForm.get('pincodeLessCtrl').addValidators(Validators.required); 
      this.bookingForm.get('pincodeCtrl').clearValidators();

      this.pincodeGreater = this.bookingForm.get('pincodeGreaterCtrl').value;
      this.pincodeLess = this.bookingForm.get('pincodeLessCtrl').value
      this.pinPerformance = ''
           
    } else {
      console.log("---- else condtion of validation ----")
      this.bookingForm.get('pincodeGreaterCtrl').clearValidators();
      this.bookingForm.get('pincodeLessCtrl').clearValidators();
      this.bookingForm.get('pincodeCtrl').addValidators(Validators.required);
      this.pincodeGreater = 0;
      this.pincodeLess = 0;
      this.pinPerformance = this.bookingForm.get('pincodeCtrl').value
    }

    // console.log("this.bookingForm.get('addressCtrl').value---",this.bookingForm.get('addressCtrl').value)

    if(this.addressEvent == 'length' || this.addressEvent == ''){
      this.bookingForm.get('addressLengthCtrl').addValidators(Validators.required); 
      this.bookingForm.get('addressCtrl').clearValidators();
      this.addressLength =  this.bookingForm.get('addressLengthCtrl').value;
      this.addressValue = '';
    }else{
      // console.log("---- else condtion of validation address----")
      this.bookingForm.get('addressLengthCtrl').clearValidators();
      this.bookingForm.get('addressCtrl').addValidators(Validators.required); 
      this.addressLength =  0;
      this.addressValue = this.bookingForm.get('addressCtrl').value;
    }

    if(this.orderHistoryEvent == 'orderHistoryGreater' || this.orderHistoryEvent == ''){
      this.bookingForm.get('orderHistoryGreaterCtrl').addValidators(Validators.required); 
      this.bookingForm.get('orderHistoryCtrl').clearValidators();
      this.orderHistoryGreater =  this.bookingForm.get('orderHistoryGreaterCtrl').value;
      this.orderHistoryValue = '';
    }else{
      // console.log("---- else condtion of validation address----")
      this.bookingForm.get('orderHistoryGreaterCtrl').clearValidators();
      this.bookingForm.get('orderHistoryCtrl').addValidators(Validators.required); 
      this.orderHistoryGreater =  0;
      this.orderHistoryValue = this.bookingForm.get('orderHistoryCtrl').value;
    }

    // console.log("this.bookingForm.get('orderStatusCtrl').value---",this.bookingForm.get('orderStatusCtrl').value)
    if(this.bookingForm.get('orderStatusCtrl').value == 'orderPercentage' || this.bookingForm.get('orderStatusCtrl').value == ''){
      this.bookingForm.get('statusGreaterCtrl').addValidators(Validators.required); 
      this.bookingForm.get('statusLessCtrl').addValidators(Validators.required); 
      this.bookingForm.get('orderStatusCtrl').clearValidators();
      this.statusGreater =  this.bookingForm.get('statusGreaterCtrl').value;
      this.statusLess =  this.bookingForm.get('statusLessCtrl').value;
      this.orderStatus = '';
    }else{
      // console.log("---- else condtion of validation order status----")
      this.bookingForm.get('statusGreaterCtrl').clearValidators();
      this.bookingForm.get('statusLessCtrl').clearValidators();
      this.bookingForm.get('orderStatusCtrl').addValidators(Validators.required); 
      this.statusGreater =  0;
      this.statusLess =  0;
      this.orderStatus = this.bookingForm.get('orderStatusCtrl').value;
    }

      console.log("this.recomm_id---",this.recomm_id)

        this.bookingData = {
          customer_type: this.bookingForm.controls.typeCtrl.value,
          customer_history: this.bookingForm.controls.historyCtrl.value,
          zone: this.bookingForm.controls.zoneCtrl.value,
          address: this.addressValue,
          address_length: this.addressLength,
          pincode_performance: this.pinPerformance,
          pincode_greater: this.pincodeGreater,
          pincode_less: this.pincodeLess,
          order_history: this.orderHistoryValue,
          order_history_greater: this.orderHistoryGreater,
          order_status: this.orderStatus,
          statusGreater: this.statusGreater,
          statusLess: this.statusLess,
          pay_type: this.bookingForm.controls.paytypeCtrl.value,
          delivery_possibility: this.bookingForm.controls.deliveryCtrl.value,
          flag: this.bookingForm.controls.flagCtrl.value,
        }
        console.log("this.bookingData--",this.bookingData)
        this.bookingApi.saveRecommendation(this.bookingData).subscribe(data => {
          this.saveRecommendation = data['data'];
          if (data['status'] == 200) {
            this._toastrService.success("Recommendation Save Successfully", 'Success', { toastClass: 'toast ngx-toastr', closeButton: true });
            this.ngOnInit();
            this.bookingForm.get('typeCtrl').reset();
            this.bookingForm.get('historyCtrl').reset();
            this.bookingForm.get('zoneCtrl').reset();
            this.bookingForm.get('addressCtrl').reset();
            this.bookingForm.get('pincodeCtrl').reset();
            this.bookingForm.get('orderHistoryCtrl').reset();
            this.bookingForm.get('orderStatusCtrl').reset();
            this.bookingForm.get('paytypeCtrl').reset();
            this.bookingForm.get('deliveryCtrl').reset();
            this.bookingForm.get('flagCtrl').reset();
            this.addNewFlag = false;
            this.editFlag = false;
            
          }else if(data['status'] == 201){
            this._toastrService.error("Already existing this rule...", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true })
          } else if(data['status'] == 401) {
            this._toastrService.error("Some Error Occured During Recommendation Creation.", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true })
          }else if(data['status'] == 400){
            this._toastrService.error("All fields are required..", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true })
          }
        });
  }

  update(){
    this.bookingForm.controls.typeCtrl.markAsDirty();
    this.bookingForm.controls.historyCtrl.markAsDirty();
    this.bookingForm.controls.zoneCtrl.markAsDirty();
    this.bookingForm.controls.addressCtrl.markAsDirty();
    this.bookingForm.controls.pincodeCtrl.markAsDirty();
    this.bookingForm.controls.orderHistoryCtrl.markAsDirty();
    this.bookingForm.controls.orderStatusCtrl.markAsDirty();
    this.bookingForm.controls.paytypeCtrl.markAsDirty();
    this.bookingForm.controls.deliveryCtrl.markAsDirty();
    this.bookingForm.controls.flagCtrl.markAsDirty();
    console.log("this.bookingForm.get('pincodeCtrl').value---",this.bookingForm.get('pincodeCtrl').value)

    if(this.bookingForm.get('pincodeCtrl').value == 'pincodePerformance' || this.bookingForm.get('pincodeCtrl').value == ''){
      this.bookingForm.get('pincodeGreaterCtrl').addValidators(Validators.required); 
      this.bookingForm.get('pincodeLessCtrl').addValidators(Validators.required); 
      this.bookingForm.get('pincodeCtrl').clearValidators();

      this.pincodeGreater = this.bookingForm.get('pincodeGreaterCtrl').value;
      this.pincodeLess = this.bookingForm.get('pincodeLessCtrl').value
      this.pinPerformance = ''
           
    } else {
      console.log("---- else condtion of validation ----")
      this.bookingForm.get('pincodeGreaterCtrl').clearValidators();
      this.bookingForm.get('pincodeLessCtrl').clearValidators();
      this.bookingForm.get('pincodeCtrl').addValidators(Validators.required);
      this.pincodeGreater = 0;
      this.pincodeLess = 0;
      this.pinPerformance = this.bookingForm.get('pincodeCtrl').value
    }

    // console.log("this.bookingForm.get('addressCtrl').value---",this.bookingForm.get('addressCtrl').value)

    if(this.addressEvent == 'length' || this.addressEvent == ''){
      this.bookingForm.get('addressLengthCtrl').addValidators(Validators.required); 
      this.bookingForm.get('addressCtrl').clearValidators();
      this.addressLength =  this.bookingForm.get('addressLengthCtrl').value;
      this.addressValue = '';
    }else{
      // console.log("---- else condtion of validation address----")
      this.bookingForm.get('addressLengthCtrl').clearValidators();
      this.bookingForm.get('addressCtrl').addValidators(Validators.required); 
      this.addressLength =  0;
      this.addressValue = this.bookingForm.get('addressCtrl').value;
    }

    if(this.orderHistoryEvent == 'orderHistoryGreater' || this.orderHistoryEvent == ''){
      this.bookingForm.get('orderHistoryGreaterCtrl').addValidators(Validators.required); 
      this.bookingForm.get('orderHistoryCtrl').clearValidators();
      this.orderHistoryGreater =  this.bookingForm.get('orderHistoryGreaterCtrl').value;
      this.orderHistoryValue = '';
    }else{
      // console.log("---- else condtion of validation address----")
      this.bookingForm.get('orderHistoryGreaterCtrl').clearValidators();
      this.bookingForm.get('orderHistoryCtrl').addValidators(Validators.required); 
      this.orderHistoryGreater =  0;
      this.orderHistoryValue = this.bookingForm.get('orderHistoryCtrl').value;
    }

    // console.log("this.bookingForm.get('orderStatusCtrl').value---",this.bookingForm.get('orderStatusCtrl').value)
    if(this.bookingForm.get('orderStatusCtrl').value == 'orderPercentage' || this.bookingForm.get('orderStatusCtrl').value == ''){
      this.bookingForm.get('statusGreaterCtrl').addValidators(Validators.required); 
      this.bookingForm.get('statusLessCtrl').addValidators(Validators.required); 
      this.bookingForm.get('orderStatusCtrl').clearValidators();
      this.statusGreater =  this.bookingForm.get('statusGreaterCtrl').value;
      this.statusLess =  this.bookingForm.get('statusLessCtrl').value;
      this.orderStatus = '';
    }else{
      // console.log("---- else condtion of validation order status----")
      this.bookingForm.get('statusGreaterCtrl').clearValidators();
      this.bookingForm.get('statusLessCtrl').clearValidators();
      this.bookingForm.get('orderStatusCtrl').addValidators(Validators.required); 
      this.statusGreater =  0;
      this.statusLess =  0;
      this.orderStatus = this.bookingForm.get('orderStatusCtrl').value;
    }

      console.log("this.recomm_id---",this.recomm_id)
      
        this.updateData = {
          _id: this.recomm_id,
          customer_type: this.bookingForm.controls.typeCtrl.value,
          customer_history: this.bookingForm.controls.historyCtrl.value,
          zone: this.bookingForm.controls.zoneCtrl.value,
          address: this.addressValue,
          address_length: this.addressLength,
          pincode_performance: this.pinPerformance,
          pincode_greater: this.pincodeGreater,
          pincode_less: this.pincodeLess,
          order_history: this.orderHistoryValue,
          order_history_greater: this.orderHistoryGreater,
          order_status: this.orderStatus,
          statusGreater: this.statusGreater,
          statusLess: this.statusLess,
          pay_type: this.bookingForm.controls.paytypeCtrl.value,
          delivery_possibility: this.bookingForm.controls.deliveryCtrl.value,
          flag: this.bookingForm.controls.flagCtrl.value,
        }
        console.log("this.updateData--",this.updateData)
        this.bookingApi.updateRecommendation(this.updateData).subscribe(data => {
          this.updateRecommendation = data['data'];
          if (data['status'] == 200) {
            this._toastrService.success("Recommendation Updated Successfully", 'Success', { toastClass: 'toast ngx-toastr', closeButton: true });
            this.ngOnInit();
            this.bookingForm.get('typeCtrl').reset();
            this.bookingForm.get('historyCtrl').reset();
            this.bookingForm.get('zoneCtrl').reset();
            this.bookingForm.get('addressCtrl').reset();
            this.bookingForm.get('pincodeCtrl').reset();
            this.bookingForm.get('orderHistoryCtrl').reset();
            this.bookingForm.get('orderStatusCtrl').reset();
            this.bookingForm.get('paytypeCtrl').reset();
            this.bookingForm.get('deliveryCtrl').reset();
            this.bookingForm.get('flagCtrl').reset();
            this.addNewFlag = false;
            this.editFlag = false;
            
          }else if(data['status'] == 201){
            this._toastrService.error("Existing, Please changes on it then update.", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true })
          } else if(data['status'] == 401) {
            this._toastrService.error("Some Error Occured During Recommendation Updation.", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true })
          }else if(data['status'] == 400){
            this._toastrService.error("All fields are required..", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true })
          }
        });
  }
  Back(){
    this.addNewFlag = false;
    this.editFlag = false;
    this.bookingForm.get('typeCtrl').reset();
    this.bookingForm.get('historyCtrl').reset();
    this.bookingForm.get('zoneCtrl').reset();
    this.bookingForm.get('addressCtrl').reset();
    this.bookingForm.get('pincodeCtrl').reset();
    this.bookingForm.get('orderHistoryCtrl').reset();
    this.bookingForm.get('orderStatusCtrl').reset();
    this.bookingForm.get('paytypeCtrl').reset();
    this.bookingForm.get('deliveryCtrl').reset();
    this.bookingForm.get('flagCtrl').reset();
    this.bookingForm.get('orderHistoryGreaterCtrl').reset();
    this.bookingForm.get('addressLengthCtrl').reset();
    this.bookingForm.get('pincodeGreaterCtrl').reset();
    this.bookingForm.get('pincodeLessCtrl').reset();
    this.bookingForm.get('statusGreaterCtrl').reset();
    this.bookingForm.get('statusLessCtrl').reset();
  }

  edit(_id){
    this.editFlag = true;
    this.recomm_id = _id
    console.log("_id--edit---",_id)
    this.bookingApi.getRecommendation(_id).subscribe(data => {
      this.getRecommendation = data['data'];
      if(data['status']== 200){
        console.log("getRecommendation----",this.getRecommendation)
        this.type=data['data'][0]['customer_type'];
        this.selectType(this.type);
        this.customer_history = data['data'][0]['customer_history'];
        this.zone = data['data'][0]['zone'];
        // console.log("this.zone--",this.zone)
        this.address = data['data'][0]['address'];
        this.selectAddress(this.address)
        this.pincode_performance = data['data'][0]['pincode_performance'];
        this.selectPincode(this.pincode_performance)
        this.order_history = data['data'][0]['order_history'];
        this.selectOrderHistory(this.order_history)
        this.pay_type = data['data'][0]['pay_type'];
        this.order_status = data['data'][0]['order_status'];
        this.selectOrderStatus(this.order_status)
        this.delivery_possibility = data['data'][0]['delivery_possibility'];
        this.flag = data['data'][0]['flag'];
        this.address_length = data['data'][0]['address_length'];
        this.pincode_greater = data['data'][0]['pincode_greater'];
        this.pincode_less = data['data'][0]['pincode_less'];
        this.status_greater = data['data'][0]['status_greater'];
        this.status_less = data['data'][0]['status_less'];
        this.order_history_greater = data['data'][0]['order_history_greater']
      }
    });
  }
  delete(_id){
    console.log("_id--delete---",_id)
    this.bookingApi.deleteRecommendation(_id).subscribe(data => {
      this.removeRecommendation = data['data'];
      if(data['status'] == 200){
        this._toastrService.success("Deleted Successfully!!!....", 'Success', { toastClass: 'toast ngx-toastr', closeButton: true });
        this.ngOnInit();
      }else if(data['status'] == 400){
        this._toastrService.error("Some thing is wrong!!!....", 'Error', { toastClass: 'toast ngx-toastr', closeButton: true });
      }
    });
  }

  deleteConfirmation(content) {
    this.modalService.open(content, { centered: true });
  }

  filterUpdate(event) {
    // Reset ng-select on search
    // this.zoneSelect = this.zoneSelect[0];

    const val = event.target.value.toLowerCase();

    // Filter Our Data
    const temp = this.tempData.filter(function (d) {
      return d.zone.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // Update The Rows
    this.recommendationData = temp;
    // Whenever The Filter Changes, Always Go Back To The First Page
    // this.table.offset = 0;
  }

}
