import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { CoreConfigService } from '@core/services/config.service';
import { BookingApi } from '@core/api/booking';
import { ToastrService } from 'ngx-toastr';
import { Role, User } from 'app/auth/models';

@Component({
  selector: 'app-auth-login-v2',
  templateUrl: './auth-login-v2.component.html',
  styleUrls: ['./auth-login-v2.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthLoginV2Component implements OnInit {
  //  Public
  public coreConfig: any;
  public loginForm: FormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public error = '';
  public passwordTextType: boolean;

  // Private
  private _unsubscribeAll: Subject<any>;
  email: any;
  password: any;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   *  @param {ToastrService} _toastrService
   */
   public currentUser: Observable<User>;
   private currentUserSubject: BehaviorSubject<User>;
  user: any;
  
  constructor(
    private _coreConfigService: CoreConfigService,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private bookingApi: BookingApi,
    private _toastrService: ToastrService
  ) {
    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  get isAdmin() {
    return this.currentUser && this.currentUserSubject.value.role === Role.Admin;
  }

  /**
   *  Confirms if user is client
   */
  get isClient() {
    return this.currentUser && this.currentUserSubject.value.role === Role.Client;
  }
  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  // onSubmit() {
  //   this.submitted = true;

  //   // stop here if form is invalid
  //   if (this.loginForm.invalid) {
  //     return;
  //   }

  //   // Login
  //   this.loading = true;

  //   // redirect to home page
  //   setTimeout(() => {
  //     this._router.navigate(['/']);
  //   }, 100);
  // }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    // this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
  }
  
  login(){
    this.email = this.loginForm.controls.email.value;
    this.password = this.loginForm.controls.password.value;
    this.bookingApi.login(this.email,this.password).subscribe(data => {
      // console.log("data---",JSON.stringify(data))
      if (data['status'] == 200) {
        this.user = data['data']['user']
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        console.log("success")
        this._toastrService.success('You have successfully logged in ', '👋 Welcome, ', { toastClass: 'toast ngx-toastr', closeButton: true });
        
        setTimeout(() => {
          this._router.navigate(['/home']);
        }, 100); 
      }else if(data['status'] == 401){
        this.error = 'fail, Email or Password is wrong'
        // this._toastrService.error('fail, Email or Password is wrong');
      }else if(data['status'] == 404){
        this.error = 'fail, Please provide email or password'
        // this._toastrService.error('fail, Please provide email or password');
      }
    });
  }
 
  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
    
}
